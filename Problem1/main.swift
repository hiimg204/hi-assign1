//
// Project:      assignment 1 - Problem 1
// File:         main.swift
// Author:       Ismael F. Hepp
// Date:         Oct 18, 2016
// Course:       IMG204
//
// Problem Statement:
// Write an application that displays your initials in large block letters.
// Make each large letter out of the corresponding regular character. Each
// line contains a number of characters in its line. The number must be
// properly formatted and aligned.
// For example:
//
//  FFFFFFFFFFFFFFFF    NN             NN    18
//  FFFFFFFFFFFFFFFF    NNN            NN    19
//  FF                  NNNN           NN     8
//  FF                  NN NN          NN     8
//  FF                  NN  NN         NN     8
//  FF                  NN   NN        NN     8
//  FFFFFFFF            NN    NN       NN    14
//  FFFFFFFF            NN     NN      NN    14
//  FF                  NN      NN     NN     8
//  FF                  NN       NN    NN     8
//  FF                  NN        NN   NN     8
//  FF                  NN         NN  NN     8
//  FF                  NN          NN NN     8
//  FF                  NN           NNNN     8
//  FF                  NN            NNN     7
//
// Inputs:   none
// Outputs:  requested initials in large block characters

import Foundation

print(String(format:"IIIIIIIIIIII  FFFFFFFFFFFF  HH        HH  %#2d", 28))
print(String(format:"IIIIIIIIIIII  FFFFFFFFFFFF  HH        HH  %#2d", 28))
print(String(format:"     II       FF            HH        HH  %#2d", 8))
print(String(format:"     II       FF            HH        HH  %#2d", 8))
print(String(format:"     II       FFFFFFFF      HHHHHHHHHHHH  %#2d", 22))
print(String(format:"     II       FFFFFFFF      HHHHHHHHHHHH  %#2d", 22))
print(String(format:"     II       FF            HH        HH  %#2d", 8))
print(String(format:"     II       FF            HH        HH  %#2d", 8))
print(String(format:"IIIIIIIIIIII  FF            HH        HH  %#2d", 18))
print(String(format:"IIIIIIIIIIII  FF            HH        HH  %#2d", 18))
