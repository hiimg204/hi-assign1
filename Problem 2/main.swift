//
// Project:      assignment 1 - Problem 2
// File:         main.swift
// Author:       Ismael F. Hepp
// Date:         Oct 18, 2016
// Course:       IMG204
//
// Problem Statement:
// Write an application that prompts for and reads a double value representing
// a monetary amount. Then determine the fewest number of each bill and coin 
// needed to represent that amount, starting with the highest (assume that a 
// ten-dollar bill is the maximum size needed). For example, if the value 
// entered is 47.63 (forty-seven dollars and sixty-three cents), then the 
// program should print the equivalent amount as:
//
// 4 ten dollar bills
// 1 five dollar bills
// 2 one dollar bills
// 2 quarters
// 1 dimes
// 0 nickles
// 3 pennies
//
// Inputs:  monetary value entered from keyboard
// Outputs: equivalent value expressed as bills and change

import Foundation

var bills10: Int = 0
var bills05: Int = 0
var bills01: Int = 0
var cents50: Int = 0
var cents25: Int = 0
var cents10: Int = 0
var cents05: Int = 0
var cents01: Int = 0

print("Type a monetary value: ")
var monetaryValue: Double = IOLibrary.getDoubleFromConsole()
var changeValue: Int = Int(monetaryValue * 100)

if (changeValue > 1000) {
  bills10 = changeValue/1000
  changeValue %= 1000
}

if (changeValue > 500 ) {
  bills05 = changeValue/500
  changeValue %= 500
}

if (changeValue > 100 ) {
  bills01 = changeValue/100
  changeValue %= 100
}

if (changeValue > 50 ) {
  cents50 = changeValue/50
  changeValue %= 50
}

if (changeValue > 25 ) {
  cents25 = changeValue/25
  changeValue %= 25
}

if (changeValue > 10 ) {
  cents10 = changeValue/10
  changeValue %= 10
}

if (changeValue > 5 ) {
  cents05 = changeValue/5
  changeValue %= 5
}

cents01 = changeValue

if (bills10 != 0) {
  print("\(bills10) ten dollar bills")
}

if (bills05 != 0) {
  print("\(bills05) five dollar bills")
}

if (bills01 != 0) {
  print("\(bills01) one dollar bills")
}

if (cents50 != 0) {
  print("\(cents50) half dollars")
}

if (cents25 != 0) {
  print("\(cents25) quarters")
}

if (cents10 != 0) {
  print("\(cents10) dimes")
}

if (cents05 != 0) {
  print("\(cents05) nickels")
}

if (cents01 != 0) {
  print("\(cents01) pennies")
}
